﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;

namespace DeDesign.Designs
{
    class Design: INotifyPropertyChanged
    {
		private String _imageLink;
		private String _title;
		private double _costPerMeter;
	

		public String Title
		{
			get { return _title; }
			set {
				if (_title == value)
					return;
				_title = value;
				OnPropertyChanged("CostPerMetr");
			}
		}

		public double CostPerMeter
		{
			get { return _costPerMeter; }
			set {
				if (_costPerMeter == value)
					return;
				_costPerMeter = value;
				OnPropertyChanged("CostPerMetr");
			}
		}

		public String ImageLink
		{
			get { return _imageLink; }
			set {
				if (_imageLink == value)
					return;
				_imageLink = value;
				OnPropertyChanged("CostPerMetr");
			}
		}

		public event PropertyChangedEventHandler PropertyChanged;

		protected virtual void OnPropertyChanged(string name = "")
		{
			PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(name));
		}
	}
}
