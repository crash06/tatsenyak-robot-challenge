﻿using DeDesign.Designs;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Forms;
using System.Windows.Forms.VisualStyles;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using MessageBox = System.Windows.Forms.MessageBox;

namespace DeDesign
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Page
    {
        private List<Design> _designsList;

        private BindingList<Design> _designs;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            var EdgeStyle = new DataGridTableStyle();



            _designsList = new List<Design>()
            {
                new Design() { ImageLink = "https://www.publicdomainpictures.net/pictures/320000/nahled/background-image.png", Title = "Restourant", CostPerMeter = 12.33 },
                new Design() { ImageLink = "https://www.publicdomainpictures.net/pictures/320000/nahled/background-image.png", Title = "Apartamens", CostPerMeter = 8.33 }
            };

            _designs = new BindingList<Design>(_designsList);

            designesList.ItemsSource = _designs;

            _designs.ListChanged += _designs_ListChanged;
        }

        private void _designs_ListChanged(object sender, ListChangedEventArgs e)
        {
            _designsList = new List<Design>(_designs);
            designesList.ItemsSource = _designs;
            Console.WriteLine("Called" + _designs.RaiseListChangedEvents);
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            DesignersInfo nextPage = new DesignersInfo();
            this.NavigationService.Navigate(nextPage);
            this.NavigationService.Navigate(new Uri("DesignersInfo.xaml", UriKind.Relative));
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            _designs = new BindingList<Design>(_designsList.Where((design) =>
               design.Title == SelectTitle.Text
            ).ToList());
            _designs.ListChanged += _designs_ListChanged;
        }
    }
}
