﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DeDesign
{
    /// <summary>
    /// Логика взаимодействия для DesignersInfo.xaml
    /// </summary>
    public partial class DesignersInfo : Page
    {
        public DesignersInfo()
        {
            InitializeComponent();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
                DesignersInfo nextPage = new DesignersInfo();
                this.NavigationService.Navigate(nextPage);
                this.NavigationService.Navigate(new Uri("MainWindow.xaml", UriKind.Relative));
        }
    }
}
