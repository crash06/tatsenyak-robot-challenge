﻿using System;
using System.Collections.Generic;
using System.Linq;
using LINQLib;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace LINQTest
{
    [TestClass]
    public class LINQTest
    {
        private static readonly string[] strings =  { "One", "Two", "Three", "Four", "Five" };

        private readonly List<string> stringList = new List<string>(strings);

        [TestMethod]
        public void InitListTest()
        {
            List<string> res = LINQLib.LINQClass.InitList(strings);

            Assert.IsTrue(stringList.Except(res).ToList().Count < 1);
        }

        [TestMethod]
        public void ReverseListElemsTest()
        {
            List<string> stringListExpected = stringList.ReverseEach();

            List<string> res = LINQLib.LINQClass.ListElemsReverse(stringList);

            Assert.IsTrue(stringListExpected.Except(res).ToList().Count < 1);
        }

        [TestMethod]
        public void SortListTest()
        {
            List<string> stringListExpected = LINQLib.LINQClass.InitList(strings);
            ListComparer comparer = new ListComparer();
            stringListExpected.Sort(comparer);

            List<string> res = LINQLib.LINQClass.SortList(stringList);

            Assert.IsTrue(stringListExpected.Except(res).ToList().Count < 1);
        }

        [TestMethod]
        public void ContainCharListTest()
        {
            List<string> stringListExpected = stringList.Where(elem => elem.Contains('e')).ToList();

            List<string> res = LINQLib.LINQClass.ListContainsChar(stringList, 'e');

            Assert.IsTrue(stringListExpected.Except(res).ToList().Count < 1);
        }

        [TestMethod]
        public void ListToDictionaryTest()
        {
            Dictionary<int, string> stringListExpected = stringList.Select((number, i) => new {
                i,
                number
            })
            .ToDictionary(pair => pair.i, pair => pair.number);

            Dictionary<int, string> res = LINQLib.LINQClass.ListToDictionary(stringList);

            Assert.IsTrue(stringListExpected.Except(res).ToList().Count < 1);
        }

        [TestMethod]
        public void SortListOfListsTest()
        {
            List<string> first = stringList.Where(elem => elem.Contains('e')).ToList();

            List<string> second = LINQLib.LINQClass.ListContainsChar(stringList, 'e');

            List<string> third = LINQLib.LINQClass.ListContainsChar(stringList, 'o');

            var expected = new[] { first, second, third };
            
            var res = LINQLib.LINQClass.SortArrayOfList(expected);
            
            Array.Sort<List<string>>(expected, new Comparison<List<string>>(
                (i1, i2) => i1.Count().CompareTo(i2.Count())));

            

            Assert.IsTrue(expected.Except(res).ToList().Count < 1);
        }
    }
}
