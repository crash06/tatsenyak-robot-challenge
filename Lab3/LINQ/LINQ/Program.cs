﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LINQ
{
    class Program
    {
        static void Main(string[] args)
        {
            var strings = new[] { "One", "Two", "Three", "Four", "Five" };

            List<string> numbers = LINQMethods.InitList(strings);
               
            Console.WriteLine("\nNumber List:");

            foreach (String number in numbers)
            {
                Console.WriteLine("Number = {0}", number);
            }

            Console.WriteLine("\nNumber List Reversed:");

            foreach (String number in LINQMethods.ListElemsReverse(numbers))
            {
                Console.WriteLine("Number = {0}", number);
            }

            var numbersSorted = LINQMethods.SortList(numbers);

            Console.WriteLine("\nSorted List:");

            foreach (String number in numbersSorted)
            {
                Console.WriteLine("Number = {0}", number);
            }

            var numbersFiltred = LINQMethods.ListContainsChar(numbers, 'e');

            Console.WriteLine("\nNumber contains 'e' character List:");

            foreach (String number in numbersFiltred)
            {
                Console.WriteLine("Number = {0}", number);
            }

            var numbersDictionary = LINQMethods.ListToDictionary(numbers);

            Console.WriteLine("\nNumber Dictionary:");

            foreach (KeyValuePair<int, string> kvp in numbersDictionary)
            {
                Console.WriteLine("Key = {0}, Value = {1}", kvp.Key, kvp.Value);
            }

            var lists = new[] { numbersFiltred, numbers, numbersFiltred, numbersFiltred  };

            var sortedNumbersArrays = LINQMethods.SortArrayOfList(lists);

            Console.WriteLine("\nSorted Numbers Arrays:");

            foreach (var list in sortedNumbersArrays)
            {
                Console.WriteLine("List = {0}", string.Join(",", list));
            }
        }
    }

    public static class LINQMethods { 

        public static List<string> InitList(string [] elements)
        {
            return new List<string>(elements);
        }
        
        public static List<string> ListElemsReverse(List<string> list)
        {
            return list.ReverseEach();
        }

        public static List<string> SortList(List<string> list)
        {
            ListComparer comparer = new ListComparer();
            list.Sort(comparer);
            return list;
        }

        public static List<string> ListContainsChar(List<string> list, char c)
        {
            return list.Where(number => number.Contains(c)).ToList();
        }

        public static Dictionary<int, string> ListToDictionary(List<string> list)
        {
            return list.Select((number, i) => new {
                i,
                number
            })
            .ToDictionary(pair => pair.i, pair => pair.number); 
        }

        public static List<string>[] SortArrayOfList(List<string>[] lists)
        {
            Array.Sort<List<string>>(lists, new Comparison<List<string>>(
                   (i1, i2) => i1.Count().CompareTo(i2.Count())));
            return lists;
        }
    }

    public static class ListExtension
    {
        public static List<string> ReverseEach(this List<string> list)
        {
            return list.Select(element =>
            {
                char[] charArray = element.ToCharArray();
                Array.Reverse(charArray);
                return new string(charArray);
            }).ToList();
        }
    }

    class ListComparer : IComparer<string>
    {
        public int Compare(string x, string y)
        {
            return x.CompareTo(y);
        }
    }
}
