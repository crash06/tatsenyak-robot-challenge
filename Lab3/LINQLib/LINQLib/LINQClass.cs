﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LINQLib
{
    public static class LINQClass
    {
        public static List<string> InitList(string[] elements)
        {
            return new List<string>(elements);
        }

        public static List<string> ListElemsReverse(List<string> list)
        {
            return list.ReverseEach();
        }

        public static List<string> SortList(List<string> list)
        {
            ListComparer comparer = new ListComparer();
            list.Sort(comparer);
            return list;
        }

        public static List<string> ListContainsChar(List<string> list, char c)
        {
            return list.Where(number => number.Contains(c)).ToList();
        }

        public static Dictionary<int, string> ListToDictionary(List<string> list)
        {
            return list.Select((number, i) => new {
                i,
                number
            })
            .ToDictionary(pair => pair.i, pair => pair.number); ;
        }

        public static List<string>[] SortArrayOfList(List<string>[] lists)
        {
            Array.Sort<List<string>>(lists, new Comparison<List<string>>(
                   (i1, i2) => i1.Count().CompareTo(i2.Count())));
            return lists;
        }
    }

    public static class ListExtension
    {
        public static List<string> ReverseEach(this List<string> list)
        {
            return list.Select(element =>
            {
                char[] charArray = element.ToCharArray();
                Array.Reverse(charArray);
                return new string(charArray);
            }).ToList();
        }
    }

    public class ListComparer : IComparer<string>
    {
        public int Compare(string x, string y)
        {
            return x.CompareTo(y);
        }
    }
}
