﻿using Robot.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;


namespace TatsenyakPetro
{
    public class TatsenyakPetroAlgoritm : IRobotAlgorithm
    {
        public int MyRobots = 10;

        public int RoundNumber = 0;

        public string Author => "Tatsenyak Petro";

        public TatsenyakPetroAlgoritm()
        {
            Logger.OnLogRound += Logger_OnLogRound;
        }

        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            RoundNumber++;
        }


        public int GetDistance(Position first, Position second)
        {
            return (int)(Math.Pow(first.X - second.X, 2) + Math.Pow(first.Y - second.Y, 2));
        }


        public EnergyStation[] FindClosestStantion(Robot.Common.Robot robot, Map map)
        {
            EnergyStation[] stations = map.Stations.ToArray();
            Array.Sort(stations, (stantion1, stantion2) => GetDistance(robot.Position, stantion1.Position) - GetDistance(robot.Position, stantion2.Position));
            return stations;
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            Robot.Common.Robot movingRobot = robots[robotToMoveIndex];
            if ((movingRobot.Energy > 500) && (this.MyRobots < 30) && (this.RoundNumber < 25))
            {
                this.MyRobots++;
                return new CreateNewRobotCommand() { NewRobotEnergy = 150 };
            }

            foreach (var station in map.Stations)
                if (movingRobot.Position == station.Position)
                {
                    if (station.Energy > 50)
                        return new CollectEnergyCommand();
                }


            var stantionPositions = FindClosestStantion(movingRobot, map);

            int step = this.RoundNumber > 30 ? 2 : 1;

            foreach (var station in stantionPositions)
            {
                var occupiedStation = robots.Where(robot => robot.Position == station.Position).ToArray();

                if ((occupiedStation.Length > 0))
                {
                    continue;
                }

                var energyToStation = GetDistance(movingRobot.Position, station.Position);

                if ((energyToStation <= movingRobot.Energy))
                {
                    return new MoveCommand() { NewPosition = station.Position };
                }

                int X = movingRobot.Position.X;

                if (station.Position.X > movingRobot.Position.X)
                    X += step;
                if (station.Position.X < movingRobot.Position.X)
                    X -= step;

                int Y = movingRobot.Position.Y;

                if (station.Position.Y > movingRobot.Position.Y)
                    Y += step;
                if (station.Position.Y < movingRobot.Position.Y)
                    Y -= step;

                return new MoveCommand()
                {
                    NewPosition = new Position(X, Y)
                };
            }

            return new CollectEnergyCommand();
        }
    }
}
