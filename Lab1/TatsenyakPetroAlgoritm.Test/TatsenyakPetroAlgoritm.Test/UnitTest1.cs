﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Collections.Generic;
using Robot.Common;

namespace TatsenyakPetroAlgoritm.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void MoveToStationTest()
        {
            var algo = new TatsenyakPetro.TatsenyakPetroAlgoritm();
            var map = new Map();
            var StationPosition = new Position(10, 10);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = StationPosition, RecoveryRate = 200 });       
            var closestRobot  = new Position(10, 15);          
            var robots = new List<Robot.Common.Robot>()
            { new Robot.Common.Robot() { Energy = 150, Position = closestRobot, OwnerName = "Petro"} };
            var command = algo.DoStep(robots, 0, map);
            robots[0].Position = ((MoveCommand)command).NewPosition;
            Assert.AreEqual(robots[0].Position, StationPosition);
        }

        [TestMethod]
        public void MoveByOneTest()
        {
            var algo = new TatsenyakPetro.TatsenyakPetroAlgoritm();
            var map = new Map();
            var StationPosition = new Position(41, 41);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = StationPosition, RecoveryRate = 200 });
            var closestRobot = new Position(10, 10);
            var robots = new List<Robot.Common.Robot>()
            { new Robot.Common.Robot() { Energy = 10, Position = closestRobot, OwnerName = "Petro"} };
            var command = algo.DoStep(robots, 0, map);
            robots[0].Position = ((MoveCommand)command).NewPosition;
            Assert.AreEqual(robots[0].Position, new Position(11,11));
        }

        [TestMethod]
        public void StraightMovementTest()
        {
            var algo = new TatsenyakPetro.TatsenyakPetroAlgoritm();
            var map = new Map();
            var StationPosition = new Position(11, 41);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = StationPosition, RecoveryRate = 200 });
            var closestRobot = new Position(11, 10);
            var robots = new List<Robot.Common.Robot>()
            { new Robot.Common.Robot() { Energy = 10, Position = closestRobot, OwnerName = "Petro"} };
            var command = algo.DoStep(robots, 0, map);
            robots[0].Position = ((MoveCommand)command).NewPosition;
            Assert.AreEqual(robots[0].Position, new Position(11, 11));
        }

        [TestMethod]
        public void CollectEnergyTest()
        {
            var algo = new TatsenyakPetro.TatsenyakPetroAlgoritm();
            var map = new Map();
            var StationPosition = new Position(10, 10);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = StationPosition, RecoveryRate = 200 });
            var closestRobot = new Position(10, 10);
            var robots = new List<Robot.Common.Robot>()
            { new Robot.Common.Robot() { Energy = 150, Position = closestRobot, OwnerName = "Petro"} };
            var command = algo.DoStep(robots, 0, map);
            Assert.IsTrue(command is CollectEnergyCommand);
        }

        [TestMethod]
        public void CreateRobotTest()
        {
            var algo = new TatsenyakPetro.TatsenyakPetroAlgoritm();
            var map = new Map();
            var StationPosition = new Position(10, 10);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = StationPosition, RecoveryRate = 200 });
            var closestRobot = new Position(10, 10);
            var robots = new List<Robot.Common.Robot>()
            { new Robot.Common.Robot() { Energy = 600, Position = closestRobot, OwnerName = "Petro"} };
            var command = algo.DoStep(robots, 0, map);
            Assert.IsTrue(command is CreateNewRobotCommand);
        }

        [TestMethod]
        public void EnoughtRobotsTest()
        {
            var algo = new TatsenyakPetro.TatsenyakPetroAlgoritm();
            var map = new Map();
            var StationPosition = new Position(10, 10);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = StationPosition, RecoveryRate = 200 });
            var closestRobot = new Position(10, 10);
            var robots = new List<Robot.Common.Robot>()
            { new Robot.Common.Robot() { Energy = 600, Position = closestRobot, OwnerName = "Petro"} };
            algo.MyRobots = 30;
            var command = algo.DoStep(robots, 0, map);
            Assert.IsFalse(command is CreateNewRobotCommand);
        }

        [TestMethod]
        public void GetDistanceTest()
        {
            var algo = new TatsenyakPetro.TatsenyakPetroAlgoritm();
            var position1 = new Position(10, 10);
            var position2 = new Position(10, 15);
            var distance = algo.GetDistance(position1, position2);
            Assert.AreEqual(distance, 25);
        }

        [TestMethod]
        public void GetDistanceBetweenSamePositionTest()
        {
            var algo = new TatsenyakPetro.TatsenyakPetroAlgoritm();
            var position = new Position(10, 10);
            var distance = algo.GetDistance(position, position);
            Assert.AreEqual(distance, 0);
        }

        [TestMethod]
        public void GetDistanceToClosestStantionTest()
        {
            var algo = new TatsenyakPetro.TatsenyakPetroAlgoritm();
            var map = new Map();
            var StationPosition = new Position(10, 10);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = StationPosition, RecoveryRate = 200 });
            var closestRobot = new Position(15, 15);
            var distance = algo.GetDistance(closestRobot, map.Stations[0].Position);
            Assert.AreEqual(distance, 50);
        }

        [TestMethod]
        public void FindClosestStantionTest()
        {
            var algo = new TatsenyakPetro.TatsenyakPetroAlgoritm();
            var map = new Map();
            var closestStation = new EnergyStation() { Energy = 1000, Position = new Position(10, 15), RecoveryRate = 200 };
            map.Stations.Add(closestStation);
            map.Stations.Add(new EnergyStation() { Energy = 1000, Position = new Position(35, 70), RecoveryRate = 200 });
            var closestRobot = new Position(15, 15);
            var robots = new List<Robot.Common.Robot>()
            { new Robot.Common.Robot() { Energy = 600, Position = closestRobot, OwnerName = "Petro"} };
            var stantions = algo.FindClosestStantion(robots[0], map);
            Assert.AreEqual(stantions[0], closestStation);
        }
    }
}
